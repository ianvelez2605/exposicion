﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExposicionTerminadaDos.Views;
using ExposicionTerminadaDos.Presenter;
//Nombre: Ian Antonio Velez Moreira
//Fecha: 11/02/2021
namespace ExposicionTerminadaDos
{
    public partial class Form1 : Form, IRectangulo
    {
        public Form1()
        {
            InitializeComponent();
        }
        //llamando a la interfaz
        public string longitudText
        {
            get
            {
                return txtlongitud.Text;
            }
            set
            {
                txtlongitud.Text = value;
            }
        }
        //llamando a la interfaz
        public string AmplitudText
        {
            get
            {
                return txtamplitud.Text;
            }
            set
            {
                txtamplitud.Text = value;
            }
        }
        //llamando a la interfaz
        public string AreaText 
        {
            get
            {
                return txtarea.Text;
            }
            set
            {
                txtarea.Text = value+" Sq CM";
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        //Llamar operaciones permitidas de la interfaz de usuario
        //Boton calcular
        private void button1_Click(object sender, EventArgs e)
        {
            RectanguloPresenter presenter = new RectanguloPresenter(this);
            presenter.CalcularArea();
        }
        //Boton salir
        private void button1_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea Salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        //Boton Limpiar
        private void button2_Click(object sender, EventArgs e)
        {
            txtlongitud.Clear();
            txtamplitud.Clear();
            txtarea.Clear();
        }
    }
}
