﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Nombre: Ian Antonio Velez Moreira
//Fecha: 11/02/2021
namespace ExposicionTerminadaDos.Views
{
    //La vista es una interfaz pasiva que exhibe datos (el modelo) y órdenes de usuario de las rutas (eventos) al presentador para actuar sobre los datos 
    //Interfaz
    public interface IRectangulo
    {
        //text de longitud
        string longitudText { get; set; }
        //text de amplitud
        string AmplitudText { get; set; }
        //text de área
        string AreaText { get; set; }
    }
}
