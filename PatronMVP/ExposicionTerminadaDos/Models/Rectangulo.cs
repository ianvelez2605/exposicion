﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Nombre: Ian Antonio Velez Moreira
//Fecha: 11/02/2021
namespace ExposicionTerminadaDos.Models
{
    public class Rectangulo
    {
        //El modelo es una interfaz que define los datos que mostrará o no actuado en la interfaz de usuario
        //El modelo envia al presentador los datos y notifica cambios
        //
        //Datos:
        //
        //Propiedades
        public double Longitud { get; set; }
        public double Amplitud { get; set; }
        //Método Calcular area
        public double CalcularArea()
        {
            return Longitud * Amplitud;
        }
    }
}
