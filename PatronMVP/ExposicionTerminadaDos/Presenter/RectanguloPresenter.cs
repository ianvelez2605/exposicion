﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExposicionTerminadaDos.Models;
using ExposicionTerminadaDos.Views;
//Nombre: Ian Antonio Velez Moreira
//Fecha: 11/02/2021
namespace ExposicionTerminadaDos.Presenter
{
    public class RectanguloPresenter
    {
        //El presentador actua sobre el modelo y la vista.
        //Recupera los datos del repositorio (modelo), y lo formatea para mostrarlo en la vista.
        //
        //Vista y Modelo.
        //Crear objeto a partir del modelo
        IRectangulo rectanguloView;
        //Crear constructor
        public RectanguloPresenter(IRectangulo view)
        {
            rectanguloView = view;
        }
        //Operaciones Permitidas
        public void CalcularArea()
        {
            //Instanciar
            Rectangulo rectangulo = new Rectangulo();
            rectangulo.Longitud = double.Parse(rectanguloView.longitudText);
            rectangulo.Amplitud = double.Parse(rectanguloView.AmplitudText);
            rectanguloView.AreaText = rectangulo.CalcularArea().ToString();
        }
    }
}
